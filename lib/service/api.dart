import 'package:dio/dio.dart';

class API {
  final dio = Dio();

  final String baseURL = 'https://nominatim.openstreetmap.org';

  Future<Response> getNameByLatLong(double lat, double long) async {
    Map<String, dynamic> queryParameters = {
      'format': 'jsonv2',
      'lat': lat,
      'lon': long,
    };
    return await dio.get('${baseURL}/reverse',
        queryParameters: queryParameters);
  }

  Future<Response> getLocationByAddress(String address) async {
    Map<String, dynamic> queryParameters = {'q': address, 'format': 'json'};
    return await dio.get('${baseURL}/search', queryParameters: queryParameters);
  }

  Future<Response> getRoadByLatLong(
      {required String point1, required String point2, int? driveBy}) async {
    String vehicle = driveBy == 2 ? 'car' : 'bike';
    return await dio.get(
        'https://graphhopper.com/api/1/route?vehicle=$vehicle&locale=vi&key=LijBPDQGfu7Iiq80w3HzwB4RUDJbMbhs6BU0dEnn&elevation=false&instructions=true&turn_costs=false&point=${point1}&point=${point2}');
  }
}
