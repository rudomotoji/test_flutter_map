import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget? titleWidget;
  final Widget? actionWidget;

  const CustomAppBar({this.titleWidget, this.actionWidget});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: titleWidget ?? Text(''),
      actions: [actionWidget ?? Container()],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
