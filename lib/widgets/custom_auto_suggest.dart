import 'dart:async';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:untitled1/models/address_model.dart';
import 'package:untitled1/service/api.dart';

class CustomAutocomplete extends StatefulWidget {
  final Function onSelect;

  const CustomAutocomplete({Key? key, required this.onSelect})
      : super(key: key);

  @override
  State<CustomAutocomplete> createState() => _CustomAutocompleteState();
}

class _CustomAutocompleteState extends State<CustomAutocomplete> {
  final TextEditingController _textEditingController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final GlobalKey _autocompleteKey = GlobalKey();

  ValueNotifier<List<AddressModel>> data = ValueNotifier([]);
  Timer? _timer;

  @override
  void dispose() {
    _timer?.cancel(); // Hủy bỏ timer nếu nó đang chạy
    super.dispose();
  }

  GlobalKey<AutoCompleteTextFieldState<AddressModel>> key =
      GlobalKey<AutoCompleteTextFieldState<AddressModel>>();

  void clear() {
    _textEditingController.clear();
  }

  Future<void> searchAddress(String address) async {
    var res = await API().getLocationByAddress(address);
    List<AddressModel> newData = (res.data as List<dynamic>)
        .map((e) => AddressModel.fromJson(e))
        .toList();
    data.value = newData;
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<List<AddressModel>>(
      valueListenable: data,
      builder: (ctx, addresses, child) {
        return RawAutocomplete<AddressModel>(
          optionsBuilder: (textEditingValue) => addresses,
          onSelected: (AddressModel selection) {},
          fieldViewBuilder: (BuildContext context,
              TextEditingController textEditingController,
              FocusNode focusNode,
              VoidCallback onFieldSubmitted) {
            return TextField(
              controller: _textEditingController,
              focusNode: focusNode,
              onChanged: (value) {
                if (_timer?.isActive ?? false) {
                  _timer?.cancel(); // Hủy bỏ timer nếu nó đang chạy
                }
                _timer = Timer(const Duration(seconds: 2), () {
                  searchAddress(value ?? '');
                });
              },
              decoration: const InputDecoration(
                hintText: 'Enter text',
              ),
            );
          },
          optionsViewBuilder: (BuildContext context,
              AutocompleteOnSelected<AddressModel> onSelected,
              Iterable<AddressModel> options) {
            final RenderBox renderBox = context.findRenderObject() as RenderBox;
            return Align(
              alignment: Alignment.topLeft,
              child: Material(
                elevation: 4.0,
                child: SizedBox(
                  height: 300.0,
                  child: ListView(
                    children: options
                        .map((AddressModel option) => InkWell(
                              onTap: () {
                                onSelected(option);
                              },
                              child: ListTile(
                                title: Card(
                                    child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'zlxkjclkzjxlckjzlkxcjlkzjxlckjzlkxc',
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.pink),
                                  ),
                                )),
                              ),
                            ))
                        .toList(),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
