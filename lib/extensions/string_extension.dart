import 'package:google_polyline_algorithm/google_polyline_algorithm.dart';
import 'package:latlong2/latlong.dart';

extension ExtString on String {
  List<LatLng> stringToLatLngPoints() {
    return decodePolyline(
      this,
    )
        .map((e) => LatLng(
              e.first.toDouble(),
              e.last.toDouble(),
            ))
        .toList();
  }
}
