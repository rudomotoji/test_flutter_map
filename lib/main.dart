import 'package:flutter/material.dart';
import 'package:untitled1/pages/SearchPlace.dart';
import 'package:untitled1/pages/animated_map_controller.dart';
import 'package:untitled1/pages/booking/booking.dart';
import 'package:untitled1/pages/booking/map.dart';
import 'package:untitled1/pages/booking/road_map.dart';
import 'package:untitled1/pages/circle.dart';
import 'package:untitled1/pages/custom_crs/custom_crs.dart';
import 'package:untitled1/pages/epsg3413_crs.dart';
import 'package:untitled1/pages/epsg4326_crs.dart';
import 'package:untitled1/pages/fallback_url_network_page.dart';
import 'package:untitled1/pages/home.dart';
import 'package:untitled1/pages/interactive_test_page.dart';
import 'package:untitled1/pages/latlng_to_screen_point.dart';
import 'package:untitled1/pages/list_place.dart';
import 'package:untitled1/pages/many_markers.dart';
import 'package:untitled1/pages/map_controller.dart';
import 'package:untitled1/pages/map_inside_listview.dart';
import 'package:untitled1/pages/markers.dart';
import 'package:untitled1/pages/moving_markers.dart';
import 'package:untitled1/pages/offline_map.dart';
import 'package:untitled1/pages/overlay_image.dart';
import 'package:untitled1/pages/plugin_scalebar.dart';
import 'package:untitled1/pages/plugin_zoombuttons.dart';
import 'package:untitled1/pages/point_to_latlng.dart';
import 'package:untitled1/pages/polygon.dart';
import 'package:untitled1/pages/polyline.dart';
import 'package:untitled1/pages/reset_tile_layer.dart';
import 'package:untitled1/pages/secondary_tap.dart';
import 'package:untitled1/pages/sliding_map.dart';
import 'package:untitled1/pages/stateful_markers.dart';
import 'package:untitled1/pages/tile_builder_example.dart';
import 'package:untitled1/pages/tile_loading_error_handle.dart';
import 'package:untitled1/pages/wms_tile_layer.dart';
import 'package:url_strategy/url_strategy.dart';

void main() {
  setPathUrlStrategy();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'flutter_map Demo',
      theme: ThemeData(
        useMaterial3: true,
        colorSchemeSeed: const Color(0xFF8dea88),
      ),
      // home: const HomePage(),
      initialRoute: PolygonPage.route,
      routes: <String, WidgetBuilder>{
        PolylinePage.route: (context) => const PolylinePage(),
        MapControllerPage.route: (context) => const MapControllerPage(),
        // AnimatedMapControllerPage.route: (context) =>
        //     const AnimatedMapControllerPage(),
        // MarkerPage.route: (context) => const MarkerPage(),
        // PluginScaleBar.route: (context) => const PluginScaleBar(),
        PluginZoomButtons.route: (context) => const PluginZoomButtons(),
        OfflineMapPage.route: (context) => const OfflineMapPage(),
        MovingMarkersPage.route: (context) => const MovingMarkersPage(),
        CirclePage.route: (context) => const CirclePage(),
        OverlayImagePage.route: (context) => const OverlayImagePage(),
        PolygonPage.route: (context) => const PolygonPage(),
        SlidingMapPage.route: (_) => const SlidingMapPage(),
        WMSLayerPage.route: (context) => const WMSLayerPage(),
        CustomCrsPage.route: (context) => const CustomCrsPage(),
        TileLoadingErrorHandle.route: (context) =>
            const TileLoadingErrorHandle(),
        TileBuilderPage.route: (context) => const TileBuilderPage(),
        // InteractiveTestPage.route: (context) => const InteractiveTestPage(),
        ManyMarkersPage.route: (context) => const ManyMarkersPage(),
        StatefulMarkersPage.route: (context) => const StatefulMarkersPage(),
        MapInsideListViewPage.route: (context) => const MapInsideListViewPage(),
        ResetTileLayerPage.route: (context) => const ResetTileLayerPage(),
        EPSG4326Page.route: (context) => const EPSG4326Page(),
        EPSG3413Page.route: (context) => const EPSG3413Page(),
        PointToLatLngPage.route: (context) => const PointToLatLngPage(),
        LatLngScreenPointTestPage.route: (context) =>
            const LatLngScreenPointTestPage(),
        FallbackUrlNetworkPage.route: (context) =>
            const FallbackUrlNetworkPage(),
        SecondaryTapPage.route: (context) => const SecondaryTapPage(),
        SearchPlace.route: (context) => const SearchPlace(),
        BookingScreen.route: (context) => const BookingScreen(),
        MapScreen.route: (context) => const MapScreen(),
        RoadMapScreen.route: (context) => const RoadMapScreen(),
        ListPlaceScreen.route: (context) => const ListPlaceScreen(),
      },
    );
  }
}
