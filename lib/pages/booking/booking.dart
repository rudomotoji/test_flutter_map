import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';
import 'package:sliding_sheet/sliding_sheet.dart';
import 'package:untitled1/models/detail_road.dart';
import 'package:untitled1/pages/booking/map.dart';
import 'package:untitled1/pages/booking/road_map.dart';
import 'package:untitled1/service/api.dart';
import 'package:untitled1/widgets/drawer.dart';

class BaseModel {
  final String label;
  final int value;

  BaseModel(this.label, this.value);
}

List<BaseModel> options = [
  BaseModel('xe máy', 1),
  BaseModel('xe hơi', 2),
];

class BookingScreen extends StatefulWidget {
  static const String route = 'BookingScreen';

  const BookingScreen({super.key});

  @override
  State<BookingScreen> createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {
  BaseModel dropdownValue = options.first;
  TextEditingController placeFromController = TextEditingController();
  TextEditingController placeToController = TextEditingController();
  TextEditingController driverController = TextEditingController();

  LatLng? fromPlace;
  LatLng? toPlace;

  late ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context);
  }

  void showAsBottomSheet() async {
    final result = await showSlidingBottomSheet(context, builder: (context) {
      return SlidingSheetDialog(
        elevation: 8,
        cornerRadius: 16,
        snapSpec: const SnapSpec(
          snap: true,
          snappings: [0.4, 0.7, 1.0],
          positioning: SnapPositioning.relativeToAvailableSpace,
        ),
        builder: (context, state) {
          return Container(
            height: 400,
            width: MediaQuery.of(context).size.width,
            child: Material(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Text(
                    'Chọn tài xế',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          );
        },
      );
    });

    print(result); // This is the result.
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Gọi xe')),
      drawer: buildDrawer(context, BookingScreen.route),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),

            // dropdown below..
            child: DropdownButton<BaseModel>(
              value: dropdownValue,
              isExpanded: true,
              icon: const Icon(Icons.arrow_drop_down),
              onChanged: (BaseModel? value) {
                setState(() {
                  dropdownValue = value!;
                });
              },
              items:
                  options.map<DropdownMenuItem<BaseModel>>((BaseModel value) {
                return DropdownMenuItem<BaseModel>(
                  value: value,
                  child: Text(value.label),
                );
              }).toList(),
            ),
          ),
          TextFormField(
            readOnly: true,
            controller: placeFromController,
            decoration: const InputDecoration(
              hintText: 'Chọn điểm đến',
              labelText: 'Chọn điểm đến',
            ),
            onTap: () async {
              Map<String, dynamic> data = {
                'placeFrom': true,
              };
              final result = await Navigator.of(context)
                  .pushNamed(MapScreen.route, arguments: data);

              if ((result as Map<String, dynamic>).containsKey('address')) {
                placeFromController.text = (result)['address'];
              }
              if ((result as Map<String, dynamic>).containsKey('location')) {
                setState(() {
                  fromPlace = (result)['location'];
                });
              }
            },
          ),
          TextFormField(
            readOnly: true,
            controller: placeToController,
            decoration: const InputDecoration(
              hintText: 'Chọn điểm tới',
              labelText: 'Chọn điểm tới',
            ),
            onTap: () async {
              Map<String, dynamic> data = {
                'placeTo': true,
              };
              final result = await Navigator.of(context)
                  .pushNamed(MapScreen.route, arguments: data);
              if ((result as Map<String, dynamic>).containsKey('address')) {
                placeToController.text = (result)['address'];
              }
              if ((result as Map<String, dynamic>).containsKey('location')) {
                setState(() {
                  toPlace = (result)['location'];
                });
              }
            },
          ),
          TextFormField(
            readOnly: true,
            controller: driverController,
            decoration: const InputDecoration(
              hintText: 'Chọn tài xế',
              labelText: 'Chọn tài xế',
            ),
            onTap: () {
              showAsBottomSheet();
            },
          ),
          GroupRadioButton(
            title: 'Loại thanh toán',
            listData: [
              BaseModel('Số dư', 1),
              BaseModel('Tiền mặt', 2),
            ],
          ),
        ]),
      ),
      bottomNavigationBar: renderBottomButton(),
    );
  }

  Widget renderBottomButton() {
    return GestureDetector(
      onTap: (fromPlace == null || toPlace == null)
          ? null
          : () async {
              String point1 =
                  '${fromPlace?.latitude.toString()},${fromPlace?.longitude.toString()}';
              String point2 =
                  '${toPlace?.latitude.toString()},${toPlace?.longitude.toString()}';
              try {
                progressDialog.show();
                var res = await API().getRoadByLatLong(
                    point1: point1,
                    point2: point2,
                    driveBy: dropdownValue.value);
                progressDialog.hide();

                if (res.data != null) {
                  DetailRoadModel detailRoadModel =
                      DetailRoadModel.fromJson(res.data);
                  Map<String, dynamic> data = {
                    "detailRoadModel": detailRoadModel,
                    "from": fromPlace,
                    "to": toPlace,
                  };
                  Navigator.of(context)
                      .pushNamed(RoadMapScreen.route, arguments: data);
                }
              } catch (e) {
                print('error => $e');
                progressDialog.hide();
              }
            },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(16),
        ),
        child: const Text(
          'Đặt xe',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

class GroupRadioButton extends StatefulWidget {
  const GroupRadioButton(
      {super.key, required this.listData, required this.title});

  final List<BaseModel> listData;
  final String title;

  @override
  State<GroupRadioButton> createState() => _GroupRadioButtonState();
}

class _GroupRadioButtonState extends State<GroupRadioButton> {
  BaseModel? item;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title),
          Wrap(
            direction: Axis.horizontal,
            spacing: 16,
            children: widget.listData.map((e) {
              bool isChecked = item?.value == e.value;
              return InkWell(
                onTap: () {
                  setState(() {
                    item = e;
                  });
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      isChecked ? Icons.circle : Icons.circle_outlined,
                      color: Colors.green,
                    ),
                    Text(e.label),
                  ],
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
