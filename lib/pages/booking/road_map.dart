import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:untitled1/models/detail_road.dart';
import 'package:untitled1/widgets/custom_appbar.dart';

class RoadMapScreen extends StatefulWidget {
  static const String route = 'RoadMapScreen';

  const RoadMapScreen({super.key});

  @override
  State<RoadMapScreen> createState() => _RoadMapScreenState();
}

class _RoadMapScreenState extends State<RoadMapScreen> {
  final hoangSa = <LatLng>[
    LatLng(15.542722, 110.881205),
    LatLng(17.481057, 111.165367),
    LatLng(17.345504, 112.461055),
    LatLng(16.744258, 113.13266),
    LatLng(15.622074, 112.744866),
    LatLng(15.393742, 111.042395),
    LatLng(15.542722, 110.881205),
  ];

  final truongSa = <LatLng>[
    LatLng(8.796728, 110.440687),
    LatLng(12.872081, 114.089415),
    LatLng(9.78571, 117.389127),
    LatLng(5.460415, 113.154463),
    LatLng(8.796728, 110.440687),
  ];

  late final MapController _mapController;

  Color seaColor = const Color(0xFFAAD3DF);

  List<Marker> allMarkers = [];
  DetailRoadModel? detailRoadModel;

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
  }

  addMarkerToMap(BuildContext context) {
    final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map<String, dynamic>;
    print('mounted arguments => $arguments');
    if (arguments.containsKey('detailRoadModel') &&
        arguments.containsKey('from') &&
        arguments.containsKey('to')) {
      LatLng from = arguments['from'];
      LatLng to = arguments['to'];
      DetailRoadModel newDetailRoadModel = arguments['detailRoadModel'];

      List<Marker> newMarkers = [];

      newMarkers.add(
        Marker(
          point: LatLng(
            from.latitude,
            from.longitude,
          ),
          builder: (context) => const Icon(
            Icons.location_pin,
            color: Colors.red,
            size: 30,
          ),
        ),
      );
      newMarkers.add(
        Marker(
          point: LatLng(
            to.latitude,
            to.longitude,
          ),
          builder: (context) => const Icon(
            Icons.location_pin,
            color: Colors.red,
            size: 30,
          ),
        ),
      );

      setState(() {
        detailRoadModel = newDetailRoadModel;
        allMarkers = newMarkers;
      });

      Future.delayed(const Duration(seconds: 1), () {
        if (newDetailRoadModel.getPaths.isNotEmpty) {
          _mapController.fitBounds(
              newDetailRoadModel.getPaths[0].getLatLngBounds,
              options: const FitBoundsOptions(
                padding: EdgeInsets.all(8.0),
                maxZoom: 15.0,
              ));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (mounted) {
      addMarkerToMap(context);
    }

    return Scaffold(
      appBar: const CustomAppBar(
        titleWidget: Text(''),
      ),
      body: Stack(
        children: [
          renderMap(),
          if (detailRoadModel != null && detailRoadModel!.getPaths.isNotEmpty)
            Positioned(
              child: renderBottomView(),
              bottom: 0,
              right: 0,
              left: 0,
            )
        ],
      ),
    );
  }

  Widget renderBottomView() {
    return Container(
      height: 200,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3), // Điều chỉnh vị trí bóng đổ
          ),
        ],
        borderRadius: const BorderRadius.all(
          Radius.circular(16),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Khoảng cách: ${detailRoadModel!.getPaths[0].getDistance} km'),
          Text('Thời gian: ${detailRoadModel!.getPaths[0].getTime()}'),
        ],
      ),
    );
  }

  Widget renderMap() {
    return FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        center: LatLng(10.801432, 106.709157),
        zoom: 13,
        interactiveFlags: InteractiveFlag.all & ~InteractiveFlag.rotate,
        minZoom: 4.0,
        maxZoom: 17.0,
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          // userAgentPackageName: 'dev.fleaflet.flutter_map.example',
        ),
        MarkerLayer(markers: allMarkers),
        PolylineLayer(
          polylines: [
            Polyline(
              points: detailRoadModel == null ||
                      (detailRoadModel != null &&
                          detailRoadModel!.getPaths.isEmpty)
                  ? []
                  : detailRoadModel!.getPaths[0].getListLatLng,
              strokeWidth: 4,
              color: Colors.red,
            )
          ],
        ),
        PolygonLayer(polygons: [
          Polygon(
            points: hoangSa,
            isFilled: true,
            color: seaColor,
          ),
          Polygon(
            points: truongSa,
            isFilled: true,
            color: seaColor,
          ),
        ]),
      ],
    );
  }
}
