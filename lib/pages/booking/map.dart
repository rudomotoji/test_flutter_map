import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:untitled1/models/location_model.dart';
import 'package:untitled1/service/api.dart';
import 'package:untitled1/widgets/custom_appbar.dart';

class MapScreen extends StatefulWidget {
  static const String route = 'MapScreen';

  const MapScreen({super.key});

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  final hoangSa = <LatLng>[
    LatLng(15.542722, 110.881205),
    LatLng(17.481057, 111.165367),
    LatLng(17.345504, 112.461055),
    LatLng(16.744258, 113.13266),
    LatLng(15.622074, 112.744866),
    LatLng(15.393742, 111.042395),
    LatLng(15.542722, 110.881205),
  ];

  final truongSa = <LatLng>[
    LatLng(8.796728, 110.440687),
    LatLng(12.872081, 114.089415),
    LatLng(9.78571, 117.389127),
    LatLng(5.460415, 113.154463),
    LatLng(8.796728, 110.440687),
  ];

  late final MapController _mapController;

  Color seaColor = const Color(0xFFAAD3DF);

  LatLng? _locationUser;

  ValueNotifier<String> locationName = ValueNotifier('');
  ValueNotifier<bool> enableButton = ValueNotifier(false);
  ValueNotifier<LatLng> locationNotifier = ValueNotifier(LatLng(0, 0));
  Timer? _timer;
  final pointSize = 40.0;
  final pointY = 200.0;
  Map<String, dynamic> dataArguments = {};

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
    _getLocation();
  }

  Future<void> _getLocation() async {
    Location location = new Location();
    location.enableBackgroundMode(enable: true);

    location.onLocationChanged.listen((LocationData currentLocation) {
      // Use current location
    });

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    try {
      print('currentLocation loading');
      _serviceEnabled = await location.serviceEnabled();
      print('_serviceEnabled => $_serviceEnabled');
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      print('_permissionGranted => $_permissionGranted');
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return;
        }
      }

      _locationData = await location.getLocation();
      print('currentLocation => $_locationData');
      locationNotifier.value =
          LatLng(_locationData.latitude ?? 0, _locationData.longitude ?? 0);
      setState(() {
        _locationUser =
            LatLng(_locationData.latitude ?? 0, _locationData.longitude ?? 0);
      });
    } catch (e) {
      print('Could not get location: $e');
    }
  }

  onSelectAddress(option) {
    print('object => $option');
  }

  void updatePoint(MapEvent? event, BuildContext context) {
    if (_timer?.isActive ?? false)
      _timer?.cancel(); // Hủy bỏ timer nếu nó đang chạy

    _timer = Timer(const Duration(seconds: 1), () {
      final pointX = _getPointX(context);
      LatLng data = _mapController.pointToLatLng(CustomPoint(pointX, pointY)) ??
          LatLng(0, 0);
      locationNotifier.value = data;
      getNameByLatLong(data);
    });
  }

  Future<void> getNameByLatLong(LatLng point) async {
    locationName.value = 'loading';
    enableButton.value = false;

    var res = await API().getNameByLatLong(point.latitude, point.longitude);
    LocationModel la = LocationModel.fromJson(res.data);

    locationName.value = la.displayName ?? '';
    enableButton.value = true;
  }

  double _getPointX(BuildContext context) {
    return MediaQuery.of(context).size.width / 2;
  }

  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map<String, dynamic>;

    if (mounted) {
      print('mounted');
      setState(() {
        dataArguments = arguments;
      });
    }

    return Scaffold(
      appBar: CustomAppBar(
        titleWidget: ValueListenableBuilder<String>(
            valueListenable: locationName,
            builder: (ctx, name, child) {
              return Text(
                name,
                style: TextStyle(fontSize: 12),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              );
            }),
        // titleWidget: CustomAutocomplete(onSelect: onSelectAddress),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'rotate',
        child: const Icon(Icons.location_on),
        onPressed: () => _getLocation(),
      ),
      body: Stack(
        children: [
          renderMap(),
          Positioned(
              top: pointY - pointSize / 2,
              left: _getPointX(context) - pointSize / 2,
              child: Icon(
                Icons.location_pin,
                size: pointSize,
                color: Colors.red,
              )),
          Positioned(
            child: renderBottomButton(),
            bottom: 10,
            left: 0,
            right: 0,
          ),
        ],
      ),
    );
  }

  Widget renderBottomButton() {
    return ValueListenableBuilder<bool>(
      valueListenable: enableButton,
      builder: (context, enable, child) {
        return InkWell(
          onTap: !enable ? null : () {
            Map<String, dynamic> newData = {
              ...dataArguments,
              'location': locationNotifier.value,
              'address': locationName.value,
            };
            Navigator.pop(context, newData);
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            margin: EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
              color: enable ? Colors.red : Colors.red.shade200,
              borderRadius: BorderRadius.circular(16),
            ),
            child: const Text(
              'Xác nhận',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
    );
  }

  Widget renderMap() {
    return FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        center: _locationUser ?? LatLng(10.801432, 106.709157),
        zoom: 13,
        interactiveFlags: InteractiveFlag.all & ~InteractiveFlag.rotate,
        minZoom: 4.0,
        maxZoom: 19.0,
        onMapEvent: (event) {
          updatePoint(null, context);
        },
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          // userAgentPackageName: 'dev.fleaflet.flutter_map.example',
        ),
        MarkerLayer(
          markers: [
            Marker(
              width: 80.0,
              height: 80.0,
              point: _locationUser ?? LatLng(0, 0),
              builder: (ctx) => Container(
                child: const Icon(
                  Icons.location_history,
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        ),
        PolygonLayer(polygons: [
          Polygon(
            points: hoangSa,
            isFilled: true,
            color: seaColor,
          ),
          Polygon(
            points: truongSa,
            isFilled: true,
            color: seaColor,
          ),
        ]),
      ],
    );
  }
}
