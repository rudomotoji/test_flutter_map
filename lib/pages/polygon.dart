import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:untitled1/models/location_model.dart';
import 'package:untitled1/service/api.dart';
import 'package:untitled1/widgets/drawer.dart';

class PolygonPage extends StatefulWidget {
  static const String route = 'polygon';

  const PolygonPage({Key? key}) : super(key: key);

  @override
  State<PolygonPage> createState() => _PolygonPageState();
}

class _PolygonPageState extends State<PolygonPage> {
  final hoangSa = <LatLng>[
    LatLng(15.542722, 110.881205),
    LatLng(17.481057, 111.165367),
    LatLng(17.345504, 112.461055),
    LatLng(16.744258, 113.13266),
    LatLng(15.622074, 112.744866),
    LatLng(15.393742, 111.042395),
    LatLng(15.542722, 110.881205),
  ];

  final truongSa = <LatLng>[
    LatLng(8.796728, 110.440687),
    LatLng(12.872081, 114.089415),
    LatLng(9.78571, 117.389127),
    LatLng(5.460415, 113.154463),
    LatLng(8.796728, 110.440687),
  ];

  late final MapController _mapController;

  LatLng? _locationUser;
  LatLng _locationMaker = LatLng(10.801432, 106.709157);

  ValueNotifier<String> locationName = ValueNotifier('');

  Color seaColor = const Color(0xFFAAD3DF);

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
    _getLocation();
  }

  Future<void> getNameByLatLong(LatLng point) async {
    locationName.value = 'loading';
    var res = await API().getNameByLatLong(point.latitude, point.longitude);
    LocationModel la = LocationModel.fromJson(res.data);

    locationName.value = la.displayName ?? '';
  }

  Future<void> _getLocation() async {
    Location location = new Location();
    location.enableBackgroundMode(enable: true);

    location.onLocationChanged.listen((LocationData currentLocation) {
      // Use current location
    });

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    try {
      print('currentLocation loading');
      _serviceEnabled = await location.serviceEnabled();
      print('_serviceEnabled => $_serviceEnabled');
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      print('_permissionGranted => $_permissionGranted');
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return;
        }
      }

      _locationData = await location.getLocation();
      print('currentLocation => $_locationData');
      setState(() {
        _locationUser =
            LatLng(_locationData.latitude ?? 0, _locationData.longitude ?? 0);
      });
    } catch (e) {
      print('Could not get location: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Polygons')),
      drawer: buildDrawer(context, PolygonPage.route),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: ValueListenableBuilder<String>(
                  valueListenable: locationName,
                  builder: (ctx, name, child) {
                    return Text('Address: ${name}');
                  }),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: ValueListenableBuilder<String>(
                  valueListenable: locationName,
                  builder: (ctx, zoom, child) {
                    return Text(
                        'Polygons: (${_locationMaker?.latitude.toStringAsPrecision(8)},${_locationMaker?.longitude.toStringAsPrecision(8)})');
                  }),
            ),
            Flexible(
              child: FlutterMap(
                mapController: _mapController,
                options: MapOptions(
                    center: _locationUser ?? LatLng(10.801432, 106.709157),
                    zoom: 13,
                    onTap: (TapPosition tapPosition, LatLng point) {
                      print('point => $point ');
                      getNameByLatLong(point);
                      setState(() {
                        _locationMaker = point;
                      });
                    }),
                children: [
                  TileLayer(
                    urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c'],
                  ),
                  MarkerLayer(
                    markers: [
                      Marker(
                        width: 80.0,
                        height: 80.0,
                        point: _locationMaker,
                        builder: (ctx) => Container(
                          child: const Icon(Icons.location_on),
                        ),
                      ),
                      Marker(
                        width: 80.0,
                        height: 80.0,
                        point: _locationUser ?? LatLng(0, 0),
                        builder: (ctx) => Container(
                          child: const Icon(
                            Icons.location_history,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ],
                  ),
                  PolygonLayer(polygons: [
                    Polygon(
                      points: hoangSa,
                      isFilled: true,
                      color: seaColor,
                    ),
                    Polygon(
                      points: truongSa,
                      isFilled: true,
                      color: seaColor,
                    ),
                  ]),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'rotate',
        child: const Icon(Icons.location_on),
        onPressed: () => _getLocation(),
      ),
    );
  }
}
