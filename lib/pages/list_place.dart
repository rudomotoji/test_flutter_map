import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:latlong2/latlong.dart';
import 'package:untitled1/pages/many_markers.dart';
import 'package:untitled1/widgets/drawer.dart';
import 'package:untitled1/widgets/example_popup.dart';

class ListPlaceScreen extends StatefulWidget {
  static const String route = 'ListPlaceScreen';

  const ListPlaceScreen({super.key});

  @override
  State<ListPlaceScreen> createState() => _ListPlaceScreenState();
}

class _ListPlaceScreenState extends State<ListPlaceScreen> {
  double doubleInRange(Random source, num start, num end) =>
      source.nextDouble() * (end - start) + start;
  List<Marker> allMarkers = [];

  int _sliderVal = maxMarkersCount ~/ 100;

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      final r = Random();
      for (var x = 0; x < maxMarkersCount; x++) {
        allMarkers.add(Marker(
          point: LatLng(
            doubleInRange(r, 37, 55),
            doubleInRange(r, -9, 30),
          ),
          builder: (context) => const Icon(
            Icons.circle,
            color: Colors.red,
            size: 12,
          ),
          rotateAlignment: AnchorAlign.top.rotationAlignment,
        ));
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('A lot of coffee')),
      drawer: buildDrawer(context, ListPlaceScreen.route),
      body: Column(
        children: [
          Slider(
            min: 0,
            max: maxMarkersCount.toDouble(),
            divisions: maxMarkersCount ~/ 10,
            label: 'Markers',
            value: _sliderVal.toDouble(),
            onChanged: (newVal) {
              _sliderVal = newVal.toInt();
              setState(() {});
            },
          ),
          Text('$_sliderVal markers'),
          Flexible(
            child: FlutterMap(
              options: MapOptions(
                center: LatLng(50, 20),
                zoom: 5,
                interactiveFlags: InteractiveFlag.all & ~InteractiveFlag.rotate,
                minZoom: 3.0,
                maxZoom: 19.0,
                // interactionOptions: InteractionOptions(
                //   flags: InteractiveFlag.all - InteractiveFlag.rotate,
                // ),
              ),
              children: [
                TileLayer(
                  urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                  userAgentPackageName: 'dev.fleaflet.flutter_map.example',
                ),
                PopupMarkerLayer(
                  options: PopupMarkerLayerOptions(
                    markers: allMarkers.sublist(
                        0, min(allMarkers.length, _sliderVal)),
                    popupDisplayOptions: PopupDisplayOptions(
                      builder: (BuildContext context, Marker marker) =>
                          ExamplePopup(marker),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
