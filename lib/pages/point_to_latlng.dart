import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:untitled1/models/location_model.dart';
import 'package:untitled1/service/api.dart';
import 'package:untitled1/widgets/drawer.dart';

class PointToLatLngPage extends StatefulWidget {
  static const String route = 'point_to_latlng';

  const PointToLatLngPage({Key? key}) : super(key: key);

  @override
  PointToLatlngPage createState() {
    return PointToLatlngPage();
  }
}

class PointToLatlngPage extends State<PointToLatLngPage> {
  late final MapController mapController = MapController();
  final pointSize = 40.0;
  final pointY = 200.0;

  LatLng? latLng;
  Timer? _timer;

  ValueNotifier<String> locationName = ValueNotifier('');

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      updatePoint(null, context);
    });
  }

  @override
  void dispose() {
    _timer?.cancel(); // Hủy bỏ timer nếu nó đang chạy
    super.dispose();
  }

  Future<void> getNameByLatLong(LatLng point) async {
    locationName.value = 'loading';

    var res = await API().getNameByLatLong(point.latitude, point.longitude);
    LocationModel la = LocationModel.fromJson(res.data);

    locationName.value = la.displayName ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('PointToLatlng')),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            heroTag: 'rotate',
            child: const Icon(Icons.rotate_right),
            onPressed: () => mapController.rotate(60),
          ),
          const SizedBox(height: 15),
          FloatingActionButton(
            heroTag: 'cancel',
            child: const Icon(Icons.cancel),
            onPressed: () => mapController.rotate(0),
          ),
        ],
      ),
      drawer: buildDrawer(context, PointToLatLngPage.route),
      body: Stack(
        children: [
          FlutterMap(
            mapController: mapController,
            options: MapOptions(
              onMapEvent: (event) {
                updatePoint(null, context);
              },
              center: LatLng(10.801432, 106.709157),
              zoom: 13,
              minZoom: 3,
            ),
            children: [
              TileLayer(
                urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                // userAgentPackageName: 'dev.fleaflet.flutter_map.example',
              ),
            ],
          ),
          Container(
              color: Colors.white,
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: ValueListenableBuilder<String>(
                    valueListenable: locationName,
                    builder: (ctx, name, child) {
                      return Text(
                        'Address: ${name}',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      );
                    }),
              ),
              Text(
                'location: (${latLng?.latitude.toStringAsPrecision(8)},${latLng?.longitude.toStringAsPrecision(8)})',
                textAlign: TextAlign.center,
              ),
                ],
              )),
          Positioned(
              top: pointY - pointSize / 2,
              left: _getPointX(context) - pointSize / 2,
              child: Icon(Icons.crop_free, size: pointSize))
        ],
      ),
    );
  }

  void updatePoint(MapEvent? event, BuildContext context) {
    if (_timer?.isActive ?? false)
      _timer?.cancel(); // Hủy bỏ timer nếu nó đang chạy

    // Chờ 3 giây
    _timer = Timer(const Duration(seconds: 3), () {
      final pointX = _getPointX(context);
      LatLng data = mapController.pointToLatLng(CustomPoint(pointX, pointY)) ??
          LatLng(0, 0);
      getNameByLatLong(data);
      setState(() {
        latLng = data;
      });
    });
  }

  double _getPointX(BuildContext context) {
    return MediaQuery.of(context).size.width / 2;
  }
}
