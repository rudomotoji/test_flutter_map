import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:untitled1/models/address_model.dart';
import 'package:untitled1/service/api.dart';
import 'package:untitled1/widgets/drawer.dart';

class SearchPlace extends StatefulWidget {
  static const String route = 'searchPlace';

  const SearchPlace({super.key});

  @override
  State<SearchPlace> createState() => _SearchPlaceState();
}

class _SearchPlaceState extends State<SearchPlace> {
  final hoangSa = <LatLng>[
    LatLng(15.542722, 110.881205),
    LatLng(17.481057, 111.165367),
    LatLng(17.345504, 112.461055),
    LatLng(16.744258, 113.13266),
    LatLng(15.622074, 112.744866),
    LatLng(15.393742, 111.042395),
    LatLng(15.542722, 110.881205),
  ];
  final truongSa = <LatLng>[
    LatLng(8.796728, 110.440687),
    LatLng(12.872081, 114.089415),
    LatLng(9.78571, 117.389127),
    LatLng(5.460415, 113.154463),
    LatLng(8.796728, 110.440687),
  ];
  Color seaColor = const Color(0xFFAAD3DF);

  ValueNotifier<String> locationName = ValueNotifier('');
  ValueNotifier<List<AddressModel>> data = ValueNotifier([]);
  LatLng _locationMaker = LatLng(10.801432, 106.709157);
  LatLng? _locationUser;

  TextEditingController controller = TextEditingController();

  late final MapController _mapController;

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
    _getLocation();
  }

  Future<void> _getLocation() async {
    Location location = new Location();
    location.enableBackgroundMode(enable: true);

    location.onLocationChanged.listen((LocationData currentLocation) {
      // Use current location
    });

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    try {
      print('currentLocation loading');
      _serviceEnabled = await location.serviceEnabled();
      print('_serviceEnabled => $_serviceEnabled');
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      print('_permissionGranted => $_permissionGranted');
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return;
        }
      }

      _locationData = await location.getLocation();
      print('currentLocation => $_locationData');
      setState(() {
        _locationUser =
            LatLng(_locationData.latitude ?? 0, _locationData.longitude ?? 0);
      });
    } catch (e) {
      print('Could not get location: $e');
    }
  }

  Future<void> getLatLongByName(address) async {
    var res = await API().getLocationByAddress(address);
    List<AddressModel> newDate = (res.data as List<dynamic>)
        .map((e) => AddressModel.fromJson(e))
        .toList();
    data.value = newDate;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Screen Search Place')),
      drawer: buildDrawer(context, SearchPlace.route),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: TextField(
                controller: controller,
                onChanged: (text) {},
                onSubmitted: (value) {
                  getLatLongByName(value);
                },
              ),
            ),
            Flexible(
              child: ValueListenableBuilder<List<AddressModel>>(
                  valueListenable: data,
                  builder: (ctx, addresses, child) {
                    return ListView.builder(
                      itemCount: addresses.length,
                      itemBuilder: (context, i) {
                        AddressModel item = addresses[i];
                        return ListTile(
                            //return new ListTile(
                            onTap: null,
                            leading: CircleAvatar(
                              backgroundColor: Colors.blue,
                              child: Text(i.toString(), style: const TextStyle(fontSize: 11),),
                            ),
                            title: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  item.displayName ?? '',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13,
                                  ),
                                ),
                                Text(
                                  '${item.lat}-${item.lon}',
                                  style: const TextStyle(
                                    fontSize: 11,
                                  ),
                                ),
                              ],
                            ));
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
