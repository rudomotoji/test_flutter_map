import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:untitled1/extensions/string_extension.dart';

class DetailRoadModel {
  List<Paths>? paths;

  DetailRoadModel({this.paths});

  DetailRoadModel.fromJson(Map<String, dynamic> json) {
    if (json['paths'] != null) {
      paths = <Paths>[];
      json['paths'].forEach((v) {
        paths!.add(Paths.fromJson(v));
      });
    }
  }

  List<Paths> get getPaths => paths ?? [];
}

class Hints {
  double? visitedNodesSum;
  double? visitedNodesAverage;

  Hints({this.visitedNodesSum, this.visitedNodesAverage});

  Hints.fromJson(Map<String, dynamic> json) {
    visitedNodesSum = json['visited_nodes.sum'];
    visitedNodesAverage = json['visited_nodes.average'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['visited_nodes.sum'] = this.visitedNodesSum;
    data['visited_nodes.average'] = this.visitedNodesAverage;
    return data;
  }
}

class Info {
  List<String>? copyrights;
  int? took;

  Info({this.copyrights, this.took});

  Info.fromJson(Map<String, dynamic> json) {
    copyrights = json['copyrights'].cast<String>();
    took = json['took'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['copyrights'] = this.copyrights;
    data['took'] = this.took;
    return data;
  }
}

class Paths {
  double? distance;
  double? weight;
  int? time;
  int? transfers;
  bool? pointsEncoded;
  List<double>? bbox;
  String? points;
  List<Instructions>? instructions;
  double? ascend;
  double? descend;
  String? snappedWaypoints;

  Paths(
      {this.distance,
      this.weight,
      this.time,
      this.transfers,
      this.pointsEncoded,
      this.bbox,
      this.points,
      this.instructions,
      this.ascend,
      this.descend,
      this.snappedWaypoints});

  Paths.fromJson(Map<String, dynamic> json) {
    distance = json['distance'];
    weight = json['weight'];
    time = json['time'];
    transfers = json['transfers'];
    pointsEncoded = json['points_encoded'];
    bbox = json['bbox'].cast<double>();
    points = json['points'];
    if (json['instructions'] != null) {
      instructions = <Instructions>[];
      json['instructions'].forEach((v) {
        instructions!.add(Instructions.fromJson(v));
      });
    }
    ascend = json['ascend'];
    descend = json['descend'];
    snappedWaypoints = json['snapped_waypoints'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['distance'] = this.distance;
    data['weight'] = this.weight;
    data['time'] = this.time;
    data['transfers'] = this.transfers;
    data['points_encoded'] = this.pointsEncoded;
    data['bbox'] = this.bbox;
    data['points'] = this.points;
    if (this.instructions != null) {
      data['instructions'] = this.instructions!.map((v) => v.toJson()).toList();
    }
    data['ascend'] = this.ascend;
    data['descend'] = this.descend;
    data['snapped_waypoints'] = this.snappedWaypoints;
    return data;
  }

  String get getPoints => points ?? '';

  List<double> get getBbox => bbox ?? [];

  String get getDistance =>
      (round((distance ?? 0) / 1000, decimals: 2)).toString();

  String getTime() {
    double totalSeconds = (time ?? 0) / 1000;

// Tính số giờ và phút
    int hours = (totalSeconds ~/ 3600);
    int minutes = ((totalSeconds % 3600) ~/ 60);

    if (hours > 0) {
      return '$hours giờ $minutes phút';
    }

    return '$minutes phút';
  }

  List<LatLng> get getListLatLng => getPoints.stringToLatLngPoints();

  LatLngBounds get getLatLngBounds => LatLngBounds(
        LatLng(getBbox[1], getBbox[0]),
        LatLng(getBbox[3], getBbox[2]),
      );
}

class Instructions {
  double? distance;
  double? heading;
  int? sign;
  List<int>? interval;
  String? text;
  int? time;
  String? streetName;
  double? lastHeading;

  Instructions(
      {this.distance,
      this.heading,
      this.sign,
      this.interval,
      this.text,
      this.time,
      this.streetName,
      this.lastHeading});

  Instructions.fromJson(Map<String, dynamic> json) {
    distance = json['distance'];
    heading = json['heading'];
    sign = json['sign'];
    interval = json['interval'].cast<int>();
    text = json['text'];
    time = json['time'];
    streetName = json['street_name'];
    lastHeading = json['last_heading'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['distance'] = this.distance;
    data['heading'] = this.heading;
    data['sign'] = this.sign;
    data['interval'] = this.interval;
    data['text'] = this.text;
    data['time'] = this.time;
    data['street_name'] = this.streetName;
    data['last_heading'] = this.lastHeading;
    return data;
  }
}
